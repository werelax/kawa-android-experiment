(define-alias GL10 javax.microedition.khronos.opengles.GL10)
(define-alias GLES20 android.opengl.GLES20)

(define (log (fstring ::string) #!rest args)
  (android.util.Log:v "kawa-hello" (apply format fstring args)))

(define (get-triangle-buffer)
  (let* ((triangle-coords (float[]  0    0.622 0
                                   -0.5 -0.311 0
                                    0.5  0.311 0))
         (bb (java.nio.ByteBuffer:allocateDirect
              (* (length triangle-coords) 4))))
    (bb:order (java.nio.ByteOrder:nativeOrder))
    (let ((vertexBuffer ::java.nio.FloatBuffer (bb:asFloatBuffer)))
      (vertexBuffer:put triangle-coords)
      (vertexBuffer:position 0)
      vertexBuffer)))

(define (load-shader type ::int code ::String)
  (let ((shader (GLES20:glCreateShader type)))
    (GLES20:glShaderSource shader code)
    (GLES20:glCompileShader shader)
    shader))

(define (create-program)
  (let* ((vertex-shader "attribute vec4 vPosition;
                         void main() {
                           gl_Position = vPosition;
                         }")

         (fragment-shader "precision mediump float;
                           uniform vec4 vColor;
                           void main() {
                             gl_FragColor = vColor;
                           }")
         (vshader (load-shader GLES20:GL_VERTEX_SHADER vertex-shader))
         (fshader (load-shader GLES20:GL_FRAGMENT_SHADER fragment-shader))
         (program (GLES20:glCreateProgram)))
    (GLES20:glAttachShader program vshader)
    (GLES20:glAttachShader program fshader)
    (GLES20:glLinkProgram program)
    program))

(define (create-triangle)
  (let ((program ::int (create-program))
        (color (float[] 0.6 0.7 0.2 1)))
    (lambda ()
      (GLES20:glUseProgram program)
      (let ((pos-handle (GLES20:glGetAttribLocation program "vPosition"))
            (color-handle (GLES20:glGetUniformLocation program "vColor")))
        (GLES20:glEnableVertexAttribArray pos-handle)
        (GLES20:glVertexAttribPointer pos-handle 3 GLES20:GL_FLOAT #f
                                      (* 4 3) (get-triangle-buffer))
        (GLES20:glUniform4fv color-handle 1 color 0)
        (GLES20:glDrawArrays GLES20:GL_TRIANGLES 0 3)
        (GLES20:glDisableVertexAttribArray pos-handle)))))

(define draw #f)

(define-simple-class renderer (android.opengl.GLSurfaceView:Renderer)
  ((onSurfaceCreated unused config) ::void
   (GLES20:glClearColor 1 0 0 1)
   (set! draw (create-triangle)))

  ((onDrawFrame unused) ::void
   (GLES20:glClear GLES20:GL_COLOR_BUFFER_BIT)
   (draw))

  ((onSurfaceChanged unused (width ::int) (height ::int)) ::void
   (GLES20:glViewport 0 0 width height)))
