(require kawa.android.renderer)

(define-syntax try-with-resources
  (syntax-rules ()
    ((try-with-resources () body ...)
     (begin body ...))
    ((try-with-resources ((var val) (vars vals) ...) body ...)
     (let ((var val))
       (try-finally
        (try-with-resources ((vars vals) ...) body ...)
        (invoke var 'close))))))

(define (msg (fstring ::string) #!rest args)
  (android.util.Log:v "kawa-hello" (apply format fstring args)))

(define (start-telnet-repl (socket ::java.net.Socket))
  (let ((lang (gnu.expr.Language:getDefaultLanguage)))
    (msg "starting telnet repl ~a ~a" socket lang)
    (kawa.TelnetRepl:serve lang socket)))

(define (start-repl port)
  (try-with-resources ((server (java.net.ServerSocket port)))
   (let loop ()
     (msg "listing on ~a (~a)" port server)
     (let ((socket (server:accept)))
       (msg "connected ~a" socket)
       (start-telnet-repl socket)
       (loop)))))

(define-variable *activity* #f)

(define (on-click ui)
  (let ((toast (android.widget.Toast:makeText
                *activity* "Fucking REPL power!"
                android.widget.Toast:LENGTH_LONG)))
    (toast:show)))

(define (test) #f)

(define-variable *handler* #f)
(define-variable *text* #f)

(define (init-kawa activity)
  (kawa.standard.Scheme:registerEnvironment)
  (set! gnu.expr.ModuleExp:compilerAvailable #f)
  (set! gnu.expr.ModuleExp:alwaysCompile #f)
  (set! *activity* activity)
  (set! *handler* on-click)
  (set! *text* #f)
  (future (start-repl 4444)))

(define (go-fullscreen activity ::android.app.Activity)
  (activity:requestWindowFeature android.view.Window:FEATURE_NO_TITLE)
  ((activity:getWindow):setFlags
   android.view.WindowManager:LayoutParams:FLAG_FULLSCREEN
   android.view.WindowManager:LayoutParams:FLAG_FULLSCREEN))

(define-simple-class hello (android.app.Activity)
  ((onCreate (savedInstanceState ::android.os.Bundle)) ::void
   (invoke-special android.app.Activity (this) 'onCreate savedInstanceState)
   (go-fullscreen (this))
   ;; launch the repl!p
   (init-kawa (this))
   ;; build something to show
   (let ((gl-surface ::android.view.View (android.opengl.GLSurfaceView
                                          (this)
                                          EGL-context-client-version: 2
                                          preserveEGLContextOnPause: #t
                                          renderer: (kawa.android.renderer))))
     ((this):setContentView gl-surface))
   ))

;; (define-simple-class hello (android.app.Activity)
;;   ((onCreate (savedInstanceState ::android.os.Bundle)) ::void
;;    (invoke-special android.app.Activity (this) 'onCreate savedInstanceState)
;;    (go-fullscreen (this))
;;    ;; launch the repl!p
;;    (init-kawa (this))
;;    ;; build something to show
;;    (let* ((text (android.widget.TextView
;;                  (this)
;;                  text: "Hi there! This is pretty cool!!"))
;;           (button (android.widget.Button
;;                    (this)
;;                    text: "Don't click me!"
;;                    on-click-listener: (lambda (v)
;;                                         (*handler* (this)))))
;;           (panel (android.widget.LinearLayout
;;                   (this)
;;                   orientation: android.widget.LinearLayout:VERTICAL
;;                   view: text
;;                   view: button)))
;;      (set! *text* text)
;;      ((this):setContentView panel))))

;; (activity hello
;;   (on-create-view
;;     (begin
;;       (future (start-repl 4444 (this)))
;;       (let ((panel
;;         (android.widget.LinearLayout (this)
;;           orientation: android.widget.LinearLayout:VERTICAL
;;           view:
;;             (android.widget.TextView (this)
;;                text: "Hello, Android REPL from Kawa Scheme!")
;;           view:
;;             (android.widget.Button (this)
;;                text: "Click Me!"
;;                on-click-listener:
;;                   (lambda  (v)
;;                     ((android.widget.Toast:makeText (this)
;;                       "Beep Bop! You clicked me!"
;; android.widget.Toast:LENGTH_LONG):show))
;;             ))))
;;         panel
;;         ))))
